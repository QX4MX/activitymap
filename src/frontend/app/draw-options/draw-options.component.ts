import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppComponent } from '../app.component';

@Component({
    selector: 'app-draw-options',
    templateUrl: './draw-options.component.html',
    styleUrls: ['./draw-options.component.scss'],
})
export class DrawOptionsComponent implements OnInit {
    sliderMax = 20000;
    sliderMin = 0;
    sliderShowTicks = false;
    sliderStep = 1;
    sliderThumbLabel = false;
    parentRef: AppComponent;
    constructor(private dialogRef: MatDialogRef<DrawOptionsComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
        this.parentRef = data.parent;
    }

    ngOnInit(): void {}

    onColorChange() {
        let hiking_color = document.getElementById('hiking-color') as HTMLInputElement;
        let running_color = document.getElementById('running-color') as HTMLInputElement;
        let biking_color = document.getElementById('biking-color') as HTMLInputElement;
        this.parentRef.colorInput = [hiking_color.value, running_color.value, biking_color.value];
    }
    onValueChange(event) {
        console.log('test');
        console.log(event);
    }
    close() {
        this.dialogRef.close();
    }
}
