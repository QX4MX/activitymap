import { TestBed } from '@angular/core/testing';

import { KomootApiService } from './komoot-api.service';

describe('KomootApiService', () => {
  let service: KomootApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KomootApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
