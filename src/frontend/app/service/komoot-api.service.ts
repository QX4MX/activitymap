import { Injectable } from '@angular/core';
import { Buffer } from 'buffer';

@Injectable({
    providedIn: 'root',
})
export class KomootApiService {
    private baseUrl;
    public username;
    public user_id;
    private authToken;

    constructor() {
        this.baseUrl = '';
        if (window.origin.includes('localhost')) {
            this.baseUrl = 'http://localhost:4000';
        } else {
            this.baseUrl = window.origin;
        }
        this.username = this.get_cookie('username');
        this.user_id = this.get_cookie('user_id');
        this.authToken = this.get_cookie('authToken');
    }

    async send_request(url: any, auth: any, critical = true) {
        const response = await fetch(url, {
            method: 'GET',
            headers: {
                Authorization: auth,
                'Content-Type': 'application/x-www-form-urlencoded',
            },
        });
        if (response.status != 200) {
            console.log(response.status + ' ' + response.statusText);
            return;
        }
        return response;
    }

    async login(email: string, password: string) {
        let loginToken = 'Basic ' + Buffer.from(email + ':' + password).toString('base64');
        let res = await this.send_request('https://api.komoot.de/v006/account/email/' + email + '/', loginToken);
        if (res) {
            let json: any = await res.json();
            this.username = json['user']['displayname'];
            this.user_id = json['username'];
            this.authToken = 'Basic ' + Buffer.from(this.user_id + ':' + json['password']).toString('base64');
            this.set_cookie('username', json['user']['displayname'], 30);
            this.set_cookie('user_id', json['username'], 30);
            this.set_cookie('authToken', this.authToken, 30);
            return this.user_id;
        } else {
            return false;
        }
    }

    logout() {
        this.delete_cookie('username');
        this.delete_cookie('user_id');
        this.delete_cookie('authToken');
        this.username = null;
        this.user_id = null;
        this.authToken = null;
    }

    async fetch_all_tour_ids() {
        if (this.authToken) {
            let url = 'https://api.komoot.de/v007/users/' + this.user_id + '/tours/';
            let tour_IDs = [];
            let has_next_page = true;
            while (has_next_page) {
                console.debug('Load page ...');
                let res = await this.send_request(url, this.authToken);
                if (res && res.status == 200) {
                    let json = await res.json();
                    has_next_page = json['_links']['next'] && json['_links']['next']['href'];
                    if (has_next_page) {
                        url = json['_links']['next']['href'];
                    }
                    for (let tour of json['_embedded']['tours']) {
                        tour_IDs.push(tour);
                    }
                    // TODO check tourtype
                } else {
                    has_next_page = false;
                }
            }
            console.debug('Found ' + tour_IDs.length + ' tours');
            return tour_IDs;
        }
    }

    async fetch_tour_by_id(tour_id) {
        if (this.authToken) {
            let url = 'https://api.komoot.de/v007/tours/' + tour_id + '?_embedded=coordinates,way_types,';
            ('surfaces,directions,participants,');
            ('timeline&directions=v2&fields');
            ('=timeline&format=coordinate_array');
            ('&timeline_highlights_fields=tips,');
            ('recommenders');
            let res = await this.send_request(url, this.authToken);
            if (res && res.status == 200) {
                let tourJSON = await res.json();
                return tourJSON;
            } else {
                console.debug('Error');
                return;
            }
        }
    }

    async fetch_tour_cover_images(tour_id) {
        if (this.authToken) {
            let url = 'https://api.komoot.de/v007/tours/' + tour_id + '/cover_images/';
            let res = await this.send_request(url, this.authToken);
            if (res && res.status == 200) {
                let json = await res.json();
                if (json._embedded) {
                    let imgUrls = [];
                    for (let i = 0; i < json._embedded.items.length; i++) {
                        let imgUrl = json._embedded.items[i].src;
                        imgUrl = imgUrl.split('?')[0];
                        imgUrls.push(imgUrl);
                    }
                    return imgUrls;
                } else {
                    return;
                }
            } else {
                console.debug('Error');
                return;
            }
        }
    }

    set_cookie(cname, cvalue, exdays) {
        const d = new Date();
        d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
        let expires = 'expires=' + d.toUTCString();
        document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
    }

    get_cookie(cname) {
        function escape(s) {
            return s.replace(/([.*+?\^$(){}|\[\]\/\\])/g, '\\$1');
        }
        var match = document.cookie.match(RegExp('(?:^|;\\s*)' + escape(cname) + '=([^;]*)'));
        return match ? match[1] : null;
    }

    delete_cookie(name) {
        document.cookie = name + '=; Max-Age=-99999999;';
    }
}
