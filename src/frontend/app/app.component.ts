import { AfterViewInit, Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import * as L from 'leaflet';
import { min } from 'rxjs';
import { DrawOptionsComponent } from './draw-options/draw-options.component';
import { ImageSliderComponent } from './image-slider/image-slider.component';
import { LoginComponent } from './login/login.component';
import { Tour } from './model/tour-model';
import { KomootApiService } from './service/komoot-api.service';
import { DrawOptions } from './model/draw-options';
import { GPXTour } from './file-handling/gpx-handler';
import { TCXTour } from './file-handling/tcx-hanlder';
import { seconds_to_hr_minutes_str } from './util';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent implements AfterViewInit {
    title = 'activity-map';

    private map: any;
    private tiles: any;
    private files: any;

    tours: Tour[] = [];
    selectedTour: Tour;

    private tilesUrl = [
        'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
        'https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}{r}.png',
    ];
    private iconDefault;
    private iconSelected;
    drawOptions: DrawOptions = new DrawOptions();
    darkMode = 0;
    setMarker = true;
    activityStats: Map<string, [number, number]> = new Map();
    activities = ['Walking / Hiking', 'Running', 'Biking', 'Other'];
    walkingSports = ['hike', 'mountaineering'];
    runningSports = ['jogging'];
    bikingSports = ['touringbicycle', 'racebike'];
    colorInput = ['#0259c5', '#7b1fa2', '#ff8000'];

    imgSliderIsOpen = false;

    constructor(public komootApi: KomootApiService, public dialog: MatDialog) {}

    ngAfterViewInit(): void {
        this.initMap();
        for (let activity of this.activities) {
            this.activityStats.set(activity, [0, 0]);
        }
    }

    darkModeToggle() {
        this.map.removeLayer(this.tiles);
        if (this.darkMode) {
            this.darkMode = 0;
        } else {
            this.darkMode = 1;
        }
        this.tiles = L.tileLayer(this.tilesUrl[this.darkMode], {
            maxZoom: 18,
            minZoom: 3,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        });

        this.tiles.addTo(this.map);
    }

    private initMap(): void {
        this.map = L.map('map', {
            center: [50, 15],
            zoom: 5,
        });
        this.tiles = L.tileLayer(this.tilesUrl[this.darkMode], {
            maxZoom: 18,
            minZoom: 3,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        });
        this.tiles.addTo(this.map);

        let iconRetinaUrl = 'assets/marker-icon-2x.png';
        let iconUrl = 'assets/marker-icon.png';
        const shadowUrl = 'assets/marker-shadow.png';
        this.iconDefault = L.icon({
            iconRetinaUrl,
            iconUrl,
            shadowUrl,
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34],
            tooltipAnchor: [16, -28],
            shadowSize: [41, 41],
        });
        L.Marker.prototype.options.icon = this.iconDefault;

        iconRetinaUrl = 'assets/marker-icon-red-2x.png';
        iconUrl = 'assets/marker-icon-red.png';
        this.iconSelected = L.icon({
            iconRetinaUrl,
            iconUrl,
            shadowUrl,
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34],
            tooltipAnchor: [16, -28],
            shadowSize: [41, 41],
        });
    }

    async onFileSelected(event: Event) {
        const target = event.target as HTMLInputElement;
        if (target.files && target.files.length > 0) {
            this.files = target.files;
            console.debug(target.files.length + ' Files selected');
            await this.extractToursFromFiles();
        }
    }

    async extractToursFromFiles() {
        for (let i = 0; i < this.files.length; i++) {
            let text = await this.files[i].text();
            let parser = new DOMParser();
            let xmlDoc = parser.parseFromString(text, 'text/xml');
            let timeinSec;
            let tour = new Tour();
            if (this.files[i].name.includes('.gpx')) {
                tour = new GPXTour().getTour(xmlDoc);
            } else if (this.files[i].name.includes('.tcx')) {
                tour = new TCXTour().getTour(xmlDoc);
            } else {
                console.log('File Type not recognized!');
            }
            let activity = xmlDoc.getElementsByTagName('Activity')[0].attributes[0].nodeValue;
            if (this.walkingSports.includes(activity)) {
                tour.activity = this.activities[0];
            }
            if (this.runningSports.includes(activity)) {
                tour.activity = this.activities[1];
            }
            if (this.bikingSports.includes(activity)) {
                tour.activity = this.activities[2];
            } else {
                tour.activity = this.activities[3];
            }
            if (tour.positions.length != 0 && this.tours.filter((t) => t.id == tour.id).length == 0) {
                tour.distance = Math.round(tour.distance * 100) / 100;
                this.tours.push(tour);
                let activityStat = this.activityStats.get(tour.activity);
                activityStat[0] += tour.distance;
                activityStat[1] += tour.duration;
            }
        }
        return;
    }

    drawTour(tour: Tour, selectAfter: boolean) {
        this.getTour(tour).then(() => {
            if (this.setMarker) {
                const marker = L.marker(tour.positions[0], {
                    riseOnHover: true,
                });
                marker.on('click', this.onTourClick.bind(this));
                marker.bindPopup('Tour:' + tour.name + ' ' + tour.timesarr[0].toLocaleString());
                marker.addTo(this.map);
                tour.marker = marker;
            }
            //
            let colorIndex = this.activities.indexOf(tour.activity);
            if (colorIndex == -1) {
                colorIndex = 0;
            }
            const polyLineTour = L.polyline(tour.positions, {
                color: this.colorInput[colorIndex],
                weight: 3,
            });
            polyLineTour.on('click', this.onTourClick.bind(this));
            polyLineTour.addTo(this.map);
            tour.polyLine = polyLineTour;
            if (selectAfter) {
                this.selectTour(tour);
            }
        });
    }
    onTourClick(e) {
        for (let tour of this.tours) {
            if (tour.marker == e.target || tour.polyLine == e.target) {
                this.selectTour(tour);
                if (tour.id) {
                    document.getElementById(tour.id).scrollIntoView({
                        behavior: 'smooth',
                        block: 'end',
                        inline: 'nearest',
                    });
                }
            }
        }
    }

    selectTour(tour: Tour) {
        if (tour.polyLine) {
            if (this.selectedTour != null && this.selectedTour.polyLine != null) {
                let colorIndex = this.activities.indexOf(this.selectedTour.activity);
                if (colorIndex == -1) {
                    colorIndex = 0;
                }
                this.selectedTour.polyLine.setStyle({
                    color: this.colorInput[colorIndex],
                });
                this.selectedTour.marker.setIcon(this.iconDefault);
                this.selectedTour.marker.closePopup();
                this.selectedTour.selected = false;
            }
            this.selectedTour = tour;
            tour.polyLine.setStyle({
                color: 'red',
            });
            tour.polyLine.bringToFront();
            tour.marker.setIcon(this.iconSelected);
            this.selectedTour.marker.openPopup();
            tour.selected = true;

            if (this.map.getZoom() < 13) {
                this.map.setView(tour.positions[0], 13);
            } else {
                this.map.setView(tour.positions[0]);
            }
        }
    }

    drawAll() {
        for (let i = 0; i < this.tours.length; i++) {
            this.drawTour(this.tours[i], false);
        }
    }

    clearList() {
        this.tours = [];
    }

    clearMap() {
        this.map.eachLayer(function (layer) {
            this.map.removeLayer(layer);
        }, this);
        const tiles = L.tileLayer(this.tilesUrl[this.darkMode], {
            maxZoom: 18,
            minZoom: 3,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        });
        tiles.addTo(this.map);
    }

    async get_all_tour_ids() {
        let res: any = await this.komootApi.fetch_all_tour_ids();
        for (let i = 0; i < res.length; i++) {
            if (
                (res[i].distance > this.drawOptions.minimumLengthInMeters &&
                    res[i].type == 'tour_recorded' &&
                    this.drawOptions.includeRecordedTours) ||
                (res[i].type == 'tour_planned' && this.drawOptions.includePlannedTours)
            ) {
                if (
                    (this.walkingSports.includes(res[i].sport) && this.drawOptions.includeWalks) ||
                    (this.runningSports.includes(res[i].sport) && this.drawOptions.includeRunningTours) ||
                    (this.bikingSports.includes(res[i].sport) && this.drawOptions.includeBikeTours)
                ) {
                    let tour = new Tour();
                    tour.from = 'komoot';
                    tour.name = res[i].name;
                    tour.id = res[i].id;
                    tour.timesarr[0] = new Date(res[i].date);
                    tour.distance = Math.round(res[i].distance) / 1000;
                    tour.duration = seconds_to_hr_minutes_str(res[i].duration);
                    if (this.walkingSports.includes(res[i].sport)) {
                        tour.activity = this.activities[0];
                    } else if (this.runningSports.includes(res[i].sport)) {
                        tour.activity = this.activities[1];
                    } else if (this.bikingSports.includes(res[i].sport)) {
                        tour.activity = this.activities[2];
                    } else {
                        tour.activity = this.activities[3];
                    }
                    console.log(tour.activity);
                    tour.elevation = [Math.round(res[i].elevation_up), Math.round(res[i].elevation_down)];
                    if (this.tours.filter((t) => t.id == tour.id).length == 0) {
                        this.tours.push(tour);
                        let activityStat = this.activityStats.get(tour.activity);
                        activityStat[0] = Math.round(activityStat[0] + tour.distance);
                        activityStat[1] += res[i].duration;
                    }
                }
            }
        }
    }

    async getTour(tour: Tour) {
        if (tour.from == 'komoot' && tour.positions.length == 0) {
            let res: any = await this.komootApi.fetch_tour_by_id(tour.id);
            for (let j = 0; j < res._embedded.coordinates.items.length; j++) {
                tour.positions.push([res._embedded.coordinates.items[j].lat, res._embedded.coordinates.items[j].lng]);
            }
            tour.img = await this.komootApi.fetch_tour_cover_images(tour.id);
        }
    }

    openImageModal(tour: Tour) {
        //TODO getImages
        if (window.innerWidth > 1600) {
            this.dialog.open(ImageSliderComponent, {
                width: '30%',
                maxWidth: '30%',
                data: {
                    imgUrls: tour.img,
                },
            });
        } else {
            this.dialog.open(ImageSliderComponent, {
                width: '75%',
                maxWidth: '75%',
                data: {
                    imgUrls: tour.img,
                },
            });
        }
    }
    openLogin(enterAnimationDuration: string, exitAnimationDuration: string) {
        this.dialog.open(LoginComponent, {
            enterAnimationDuration,
            exitAnimationDuration,
        });
    }

    openOptions(enterAnimationDuration: string, exitAnimationDuration: string) {
        this.dialog.open(DrawOptionsComponent, {
            data: {
                parent: this,
            },
            enterAnimationDuration,
            exitAnimationDuration,
        });
    }
    toTimeStr(seconds) {
        return seconds_to_hr_minutes_str(seconds);
    }
}
