import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { interval } from 'rxjs';
import { takeWhile } from 'rxjs/operators';

@Component({
    selector: 'app-image-slider',
    templateUrl: './image-slider.component.html',
    styleUrls: ['./image-slider.component.scss']
})

export class ImageSliderComponent implements OnInit {
    currentImage;
    currentIndex = 0;
    allImages = [];
    autoScrollSpeed = 5000;
    loading = true;

    constructor(private dialogRef: MatDialogRef<ImageSliderComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
        this.allImages = data.imgUrls;
        this.currentImage = this.allImages[0];

    }

    ngOnInit(): void {
    }

    plusSlides(n) {
        this.setLoading(true);
        this.currentImage = this.allImages[(this.currentIndex + n) % this.allImages.length];
        this.currentIndex = this.allImages.indexOf(this.currentImage);
    }

    mouseEnter() {
        const prev = document.getElementsByClassName("prev") as HTMLCollectionOf<HTMLElement>;
        const next = document.getElementsByClassName("next") as HTMLCollectionOf<HTMLElement>;
        for (let i = 0; i < prev.length; i++) {
            prev[i].style.backgroundColor = "rgba(0, 0, 0, 0.8)";
        }
        for (let i = 0; i < next.length; i++) {
            next[i].style.backgroundColor = "rgba(0, 0, 0, 0.8)";
        }

    }
    mouseLeave() {
        const prev = document.getElementsByClassName("prev") as HTMLCollectionOf<HTMLElement>;
        const next = document.getElementsByClassName("next") as HTMLCollectionOf<HTMLElement>;
        for (let i = 0; i < prev.length; i++) {
            prev[i].style.backgroundColor = "rgba(0, 0, 0, 0.0)";
        }
        for (let i = 0; i < next.length; i++) {
            next[i].style.backgroundColor = "rgba(0, 0, 0, 0.0)";
        }

    }
    close() {
        this.dialogRef.close();
    }

    setLoading(bool: boolean) {
        this.loading = bool;
    }

}
