import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { KomootApiService } from '../service/komoot-api.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    constructor(public komootApi: KomootApiService, private dialogRef: MatDialogRef<LoginComponent>) { }

    ngOnInit(): void {
    }

    async login() {
        let email = document.getElementById('login-email') as HTMLInputElement;
        let password = document.getElementById('login-password') as HTMLInputElement;
        if (await this.komootApi.login(email.value, password.value)) {
            this.dialogRef.close();
        }
        else {
            alert("Login failed");
        }
    }

    logout() {
        this.komootApi.logout();
    }
}
