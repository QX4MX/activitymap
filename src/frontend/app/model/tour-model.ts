export class Tour {
  from: string = '';
  id: string = '';
  name: string = 'Undefined';
  activity: string = '';
  positions: any[] = [];
  timesarr: any[] = [];
  distance: number = 0;
  elevation: [number, number];
  duration: any = '';
  marker: L.Marker;
  polyLine: L.Polyline;
  img: any;
  selected = false;
}
