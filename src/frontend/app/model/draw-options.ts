export class DrawOptions {
    minimumLengthInMeters: number = 5000;
    includeRecordedTours: boolean = true;
    includePlannedTours: boolean = false;
    includeWalks: boolean = true;
    includeRunningTours: boolean = true;
    includeBikeTours: boolean = true;
}
