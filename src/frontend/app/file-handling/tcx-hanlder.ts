import { Tour } from '../model/tour-model'
export class TCXTour {
    getTour(xmlDoc) {
        var tour = new Tour();
        tour.from = "tcx";
        tour.id = Date.now().toString();
        tour.name = xmlDoc.getElementsByTagName("Name")[0].innerHTML;
        // dist in m to km
        let allDistancesArr = xmlDoc.getElementsByTagName("DistanceMeters");
        tour.distance = Math.round(Number(allDistancesArr[allDistancesArr.length - 1].innerHTML)) / 1000;
        // time in seconds to HH:MM
        let timeinSec = Number(xmlDoc.getElementsByTagName("TotalTimeSeconds")[0].innerHTML);
        tour.duration = timeinSec;
        tour.activity = xmlDoc.getElementsByTagName("Activity")[0].attributes[0].nodeValue;



        tour.timesarr[0] = new Date(xmlDoc.getElementsByTagName("Lap")[0].attributes[0].nodeValue);
        let positions = xmlDoc.getElementsByTagName("Position");
        for (let i = 0; i < positions.length; i++) {
            if (positions[i]) {
                tour.positions.push([
                    positions[i].children[0].firstChild!.nodeValue,
                    positions[i].children[1].firstChild!.nodeValue,
                ]);
            }
        }
        return tour
    }
}