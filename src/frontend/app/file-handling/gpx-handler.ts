import { Tour } from '../model/tour-model'
import { getDistanceFromLatLonInKm } from "../util"
export class GPXTour {

    public getTour(xmlDoc) {
        var tour = new Tour();
        tour.from = "gpx";
        tour.id = Date.now().toString();
        if (xmlDoc.getElementsByTagName("name").length != 0) {
            tour.name = xmlDoc.getElementsByTagName("name")[0].innerHTML;
        }
        tour.activity = "Other";
        let allTimesArr = xmlDoc.getElementsByTagName("time");
        for (let i = 0; i < allTimesArr.length; i++) {
            tour.timesarr.push(new Date(allTimesArr[i].innerHTML));
        }
        // time in seconds to HH:MM
        let timeinSec =
            (new Date(allTimesArr[allTimesArr.length - 1].innerHTML).getTime() -
                new Date(allTimesArr[0].innerHTML).getTime()) /
            1000;
        if (timeinSec > 0) {
            tour.duration = timeinSec;

        } let positions;
        if (xmlDoc.getElementsByTagName("trkpt").length != 0) {
            positions = xmlDoc.getElementsByTagName("trkpt");
        }
        else if (xmlDoc.getElementsByTagName("wpt").length != 0) {
            positions = xmlDoc.getElementsByTagName("wpt");
        }
        for (let i = 0; i < positions.length; i++) {
            let lat1 = positions[i].attributes[0].nodeValue;
            let lon1 = positions[i].attributes[1].nodeValue;
            tour.positions.push([lat1, lon1]);
            if (i > 0) {
                let lat2 = positions[i - 1].attributes[0].nodeValue;
                let lon2 = positions[i - 1].attributes[1].nodeValue;
                tour.distance += getDistanceFromLatLonInKm(lat1, lon2, lat2, lon2)

            }
        }
        return tour
    }
}