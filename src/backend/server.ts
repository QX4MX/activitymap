import { createServer, Server } from 'http';
import * as express from 'express';
import * as compression from 'compression';
import * as cors from 'cors';
import * as path from 'path';
import { PORT } from './util/secret';


class AppServer {
    private app: express.Application = express();
    private server: Server;
    private router: any = express.Router();

    constructor() {
        this.app = express();
        this.app.set("port", PORT || 4000);
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: false }));
        this.app.use(compression());
        this.app.use(cors());

        this.server = createServer(this.app);
        //API
        this.routes();

    }

    private routes() {
        this.app.get('*.*', express.static(path.join(__dirname, '../activity-map/')));
        this.app.all('*', function (req, res) {
            res.status(200).sendFile('/', { root: path.join(__dirname, '../activity-map/') });
        });
    }

    public listen(): void {
        this.server.listen(this.app.get("port"), () => {
            console.log('Running server on port %s', this.app.get("port"));
        });
    }

}
let server = new AppServer()
server.listen();